package com.bmv.wsxmltopdf.Service;

import org.apache.catalina.filters.ExpiresFilter;
import org.springframework.stereotype.Service;
import com.bmv.wsxmltopdf.Controller.InvoiceCtrl;
import java.io.File;
import java.nio.file.Files;
import java.util.Optional;

/**
 * Clase del primer servicio.
 *
 * @author
 */
@Service
public class XmlService
{
    final static  String XML_MIME_TYPE = "application/xml";
//    final static  String XML_MIME_TYPE = "text/xml";
    ExpiresFilter.XHttpServletResponse response;

    public Optional<String> xmlService() throws Exception {
    try{
        File file = new File("/home/yuri/Descargas/xml/GPR070228780__4574_COR810714F66.xml" );

        if (file.exists()) {
            // Obtenemos el MIME TYPE del archivo
            String fileType = Files.probeContentType(file.toPath());
            if (fileType.equals(XML_MIME_TYPE))
            {
                byte[] fileContent = Files.readAllBytes(file.toPath());
                byte[] arregloByte = xmlToPDF(fileContent);
                System.out.println(arregloByte);
                response.setContentType("application/pdf");
                response.setHeader("Content-Disposition", "inline;filename=" + "PDFYuriModificado" + ".pdf");
                response.getOutputStream().write(arregloByte);
                response.flushBuffer();
                return Optional.of("Servicio Uno");
            }
        }
    }catch (Exception e){
        e.printStackTrace();
    }
        return   Optional.of( "ERROR con el Jasper");
    }

    public   byte[] xmlToPDF(byte[] arrEntrada) throws Exception {
        InvoiceCtrl invoiceCtrl= new InvoiceCtrl();
        return invoiceCtrl.xmlToPDFInvoice(arrEntrada);
    }

}
