package com.bmv.wsxmltopdf.Controller;



import com.bmv.wsxmltopdf.Service.XmlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * Clase controlador. Cuando es consumido el web service, esta clase recibe la petición HTTP y llama al servicio.
 *
 * @author
 */
@RestController
public class XmlCtrl
{
    @Autowired
    public XmlService xmlService;

    public XmlCtrl(XmlService servicio)
    {
        xmlService = servicio;
    }

    @RequestMapping(path = "/WS1", method = RequestMethod.GET)
    public Optional<String> ventasCtrlUno() throws Exception {
        return xmlService.xmlService();
    }

}