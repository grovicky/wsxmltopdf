package com.bmv.wsxmltopdf.Controller;

import com.bmv.wsxmltopdf.utils.ConfigGlobalGasProperties;
import com.bmv.wsxmltopdf.utils.CreateQr;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.util.JRLoader;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.springframework.stereotype.Controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Controller
public class InvoiceCtrl {
    static ConfigGlobalGasProperties config = new ConfigGlobalGasProperties();
    private Boolean valConcepto=false;
    public byte[] xmlToPDFInvoice(byte[] arrEntrada) throws Exception {
            byte[] xmlinvoiceA= arrEntrada;
            HashMap<String, Object> invoiceParameters = new HashMap<>();
            String xmlPath=config.getXml();
            OutputStream   os   = new FileOutputStream(xmlPath);
            os.write(arrEntrada);
            os.close();
            //-------------------------------------------------------------------------------------
            //---------inicia lectura xml ---------------------------------------------------------
            //-------------------------------------------------------------------------------------
            //*****Comprobante*****************
            String version = (readXML(xmlPath, "rootNode", "Version"));
            String serie = (readXML(xmlPath, "rootNode", "Serie"));
            String folio = (readXML(xmlPath, "rootNode", "Folio"));
            String emision_fecha = (readXML(xmlPath, "rootNode", "Fecha"));
            String FormaPago = (readXML(xmlPath, "rootNode", "FormaPago"));
            String NoCertificado = (readXML(xmlPath, "rootNode", "NoCertificado"));
            String Certificado = (readXML(xmlPath, "rootNode", "Certificado"));
            String selloCFID = (readXML(xmlPath, "rootNode", "Sello"));
            String subTotal = (readXML(xmlPath, "rootNode", "SubTotal"));
            String Moneda = (readXML(xmlPath, "rootNode", "Moneda"));
            String tipocambio = (readXML(xmlPath, "rootNode", "TipoCambio"));
            String total = (readXML(xmlPath, "rootNode", "Total"));
            String tipoDeComprobante = (readXML(xmlPath, "rootNode", "TipoDeComprobante"));
            String metodoDePago = (readXML(xmlPath, "rootNode", "MetodoPago"));
            String LugarExpedicion = (readXML(xmlPath, "rootNode", "LugarExpedicion"));
            //*****Emisor*****************
            String emisorRFC = (readXML(xmlPath, "Emisor", "Rfc"));
            String emisorNombre = (readXML(xmlPath, "Emisor", "Nombre"));
            String emisorRegimenFiscal = (readXML(xmlPath, "Emisor", "RegimenFiscal"));
            //*****Receptor**************
            String receptorRFC = (readXML(xmlPath, "Receptor", "Rfc"));
            String receptorNombre = (readXML(xmlPath, "Receptor", "Nombre"));
            //  String receptorRegimenFiscal=(readXML(xmlPath, "Receptor", "RegimenFiscal"));
            //*****Concepto****************
            Integer contador=countNode(xmlPath,"Conceptos","Concepto");
            HashMap<String,Object> listConcepto= new HashMap<>();
            if(contador>0){
                //lista de conceptos
                for(int a=0; a<=contador;a++) {

                    String NoIdentificacion = (readXML(xmlPath, "Concepto", "NoIdentificacion"));// agregado 05 febrero 2020
                    String C_CProduct = (readXML(xmlPath, "Concepto", "ClaveProdServ"));
                    String cantidad = (readXML(xmlPath, "Concepto", "Cantidad"));
                    String C_CUnidad = (readXML(xmlPath, "Concepto", "ClaveUnidad"));
                    String c_unidad = (readXML(xmlPath, "Concepto", "Unidad"));
                    String c_descripcion = (readXML(xmlPath, "Concepto", "Descripcion"));
                    String c_valorUnitario = (readXML(xmlPath, "Concepto", "ValorUnitario"));
                    String c_importe = (readXML(xmlPath, "Concepto", "Importe"));

                    listConcepto.put("NoIdentificacion",NoIdentificacion);
                    listConcepto.put("C_CProduct",C_CProduct); // *
                    listConcepto.put("c_cantidad",cantidad); // *
                    listConcepto.put("C_CUnidad",C_CUnidad); // *
                    listConcepto.put("c_unidad",c_unidad); // *
                    listConcepto.put("c_descripcion",c_descripcion); // *
                    listConcepto.put("c_valorUnitario",c_valorUnitario); // *
                    listConcepto.put("c_importe",c_importe); // *
                }
            }
            String base = (readXML(xmlPath, "Traslado", "Base"));
            String impuesto = (readXML(xmlPath, "Traslado", "Impuesto"));
            String tipoFactor = (readXML(xmlPath, "Traslado", "TipoFactor"));
            String tasaOCuota = (readXML(xmlPath, "Traslado", "TasaOCuota"));
            /* se agregaron estos campos para retencion 30/01/2020 esto es opcional segun el xml que utilice el cliente  puede venir o no venir */
            String impuestoTazaRetenido=(readXML(xmlPath,"Retencion", "TasaOCuota"));
            String totalImpuestosRetenidos=(readXML(xmlPath,"Retencion", "Importe"));
            //*****Traslados************
            String totalimpuestosTrasladados=(readXML(xmlPath, "Impuestos", "TotalImpuestosTrasladados"));
            //*****TimbreFiscalDigital**********
            String xmlns_xsi = "http://www.w3.org/2001/XMLSchema-instance"; // (readXML(xmlPath, "TimbreFiscalDigital", "xmlns:xsi"));
            String xsi_schemaLocation = "http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/cfd/TimbreFiscalDigital/TimbreFiscalDigitalv11.xsd"; // (readXML(xmlPath, "TimbreFiscalDigital", "xsi:schemaLocation"));
            String UUID = (readXML(xmlPath, "TimbreFiscalDigital", "UUID"));
            String FechaTimbrado = (readXML(xmlPath, "TimbreFiscalDigital", "FechaTimbrado"));
            String RfcProvCertif = (readXML(xmlPath, "TimbreFiscalDigital", "RfcProvCertif"));
            String NoCertificadoSAT = (readXML(xmlPath, "TimbreFiscalDigital", "NoCertificadoSAT"));
            String selloSAT = (readXML(xmlPath, "TimbreFiscalDigital", "SelloSAT"));
            String xmlbs_tfd = "http://www.sat.gob.mx/TimbreFiscalDigital"; // (readXML(xmlPath, "TimbreFiscalDigital", "xmlns:tfd"));
            String TotalEnLetra= (readXML(xmlPath, "Totales", "TotalEnLetra"));
            //*** se agregan los string a hashmap para poder crear el xml y el jasper***

        // ********** No estan en PDF ni en XML **********
        // Sucursal
        // cuentaDePago
        // usoCfdi
        // customeraddress
        // PAGE_NUMBER
        // importeLetra
        // fiveToThousand
        // notas
        // P_OBSERVACION
        //cadenaOriginal
            //********se agregaron para que el template no marque error
            invoiceParameters.put("Sucursal"," ");
            invoiceParameters.put("cuentaDePago"," ");
            invoiceParameters.put("usoCfdi"," ");
            invoiceParameters.put("customeraddress"," ");
            invoiceParameters.put("PAGE_NUMBER"," ");
            invoiceParameters.put("importeLetra",TotalEnLetra);
            //  invoiceParameters.put("fiveToThousand"," ");
            invoiceParameters.put("notas"," ");
            invoiceParameters.put("P_OBSERVACION"," ");
            invoiceParameters.put("cadenaOriginal"," ");
            //----------------
            //  invoiceParameters.put("version",version);
            invoiceParameters.put("serie",serie); // *
            invoiceParameters.put("folio",folio); // *
            invoiceParameters.put("emision_fecha",emision_fecha); // *
            invoiceParameters.put("FormaPago",FormaPago); // *
            invoiceParameters.put("noCertificado",NoCertificado);//*
            invoiceParameters.put("Certificado",Certificado);
            invoiceParameters.put("subTotal",subTotal); // *
            invoiceParameters.put("Moneda",Moneda); // *
             //  invoiceParameters.put("tipocambio",tipocambio);
            invoiceParameters.put("total",total);//*
            invoiceParameters.put("tipoDeComprobante",tipoDeComprobante); // *
            invoiceParameters.put("metodoDePago",metodoDePago); // *
            invoiceParameters.put("LugarExpedicion",LugarExpedicion); // *
            invoiceParameters.put("e_nombre",emisorNombre); // *
            invoiceParameters.put("e_rfc",emisorRFC); // *
            invoiceParameters.put("Regimen",emisorRegimenFiscal); // *
            invoiceParameters.put("r_nombre",receptorNombre); // *
            invoiceParameters.put("r_rfc",receptorRFC); // *
            /*      Se agrega la lista de los conceptos para el jasper 05/02/2020         */
            invoiceParameters.put("severity",listConcepto);
            //  invoiceParameters.put("base",(base==null || base.isEmpty() )? c_importe:base);//*
            invoiceParameters.put("impuesto",impuesto);
             //   invoiceParameters.put("tipoFactor",tipoFactor);
             //   invoiceParameters.put("tasa0Cuota",tasaOCuota);
            invoiceParameters.put("totalImpuestosTrasladados",totalimpuestosTrasladados);//*
              //  invoiceParameters.put("xmlns_tfd",xmlbs_tfd);
              //  invoiceParameters.put("xmlns_xsi",xmlns_xsi);
              //  invoiceParameters.put("xsi_schemaLocation", xsi_schemaLocation);
            invoiceParameters.put("UUID",UUID); // *
            invoiceParameters.put("FechaTimbrado",FechaTimbrado); // *
             // invoiceParameters.put("RfcProvCertif",RfcProvCertif);
            invoiceParameters.put("noCertificadoSAT",NoCertificadoSAT);//*
            invoiceParameters.put("sello",selloSAT);//*
            invoiceParameters.put("selloCFID",selloCFID);//*
            invoiceParameters.put("transmitteraddress"," ");//*
            invoiceParameters.put("totalImpuestosRetenidos",totalImpuestosRetenidos);//*
            invoiceParameters.put("impuestoTazaRetenido",impuestoTazaRetenido);//*
             // invoiceParameters.put("xmlbs_tfd",xmlbs_tfd);
            byte[] qr = Files.readAllBytes(CreateQr.getGenerateQR(File.createTempFile("qrCode", ".png"),UUID , 798, 800).toPath());
            invoiceParameters.put("qr",qr);//*
            String jasperPath=config.getJasper();// "xml/globalnetInvoice.jasper";
            return pdfJasper(jasperPath, invoiceParameters, xmlinvoiceA);
    }

    private static byte[] pdfJasper(String jasperPath, HashMap<String, Object> invoiceParameters,byte[] xmlBytes) throws IOException, JRException {
        try{
            File xmlFile = File.createTempFile("invoiceXMLTemp", ".xml");
            FileOutputStream fos = new FileOutputStream(xmlFile);
            fos.write(xmlBytes);
            fos.close();
            JasperReport jasperReport = (JasperReport)JRLoader.loadObject(new File(jasperPath));
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, invoiceParameters, new JREmptyDataSource());

            // Lo exportamos!
            File pdf = File.createTempFile("output.", ".pdf");
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));

            xmlFile.deleteOnExit();
            return JasperExportManager.exportReportToPdf(jasperPrint);
        }catch (Exception e){
            return null;
        }


    }
    private Integer countNode(String path, String node, String element)throws IOException {
              try {
               String response = "";
               String elementName;
               SAXBuilder builder = new SAXBuilder();
               File file = new File(path);
               Document document = (Document) builder.build(file);
               Element rootNode = document.getRootElement();
               if (node.equals("rootNode"))
                   response = rootNode.getAttributeValue(element);
               else {
                   List list1 = rootNode.getChildren();
                   for (Object o : list1) {
                       Element elementCFDI1 = (Element) o;
                       elementName = elementCFDI1.getName();
                       if (elementName.equals(node)) {// conceptos
                           response = elementCFDI1.getAttributeValue(element);
                           break;
                       } else {
                           return elementCFDI1.getChildren().size();
                       }

                   }
               }
           }catch (Exception e) {
                   return 0;
               }
    return 0;
    }

        private String readXML(String path, String node, String element) throws IOException {
        String response = "";
        String elementName;
         SAXBuilder builder = new SAXBuilder();
        File file = new File(path);
        try {
            Document document = (Document) builder.build(file);
            Element rootNode = document.getRootElement();
            if (node.equals("rootNode"))
                response = rootNode.getAttributeValue(element);
            else {
                List list1 = rootNode.getChildren();
                for (Object o : list1) {
                    Element elementCFDI1 = (Element) o;
                    elementName = elementCFDI1.getName();
                    if (elementName.equals(node)) {
                        response = elementCFDI1.getAttributeValue(element);
                        break;
                    } else {
                        List list2 = elementCFDI1.getChildren();
                        for (Object value : list2) {
                            Element elementCFDI2 = (Element) value;
                            elementName = elementCFDI2.getName();
                            if (elementName.equals(node)) {
                                response = elementCFDI2.getAttributeValue(element);
                                break;
                            } else {
                                List list3 = elementCFDI2.getChildren();
                                for (Object item : list3) {
                                    Element elementCFDI3 = (Element) item;
                                    elementName = elementCFDI3.getName();
                                    if (elementName.equals(node)) {
                                        response = elementCFDI3.getAttributeValue(element);
                                        break;
                                    } else {
                                        List list4 = elementCFDI3.getChildren();
                                        for (Object item2 : list4) {
                                            Element elementCFDI4 = (Element) item2;
                                            elementName = elementCFDI4.getName();
                                            if (elementName.equals(node)) {
                                                response = elementCFDI4.getAttributeValue(element);
                                                break;
                                            } else {
                                                List list5 = elementCFDI4.getChildren();
                                                for (Object item3 : list5) {
                                                    Element elementCFDI5 = (Element) item3;
                                                    elementName = elementCFDI5.getName();
                                                    if (elementName.equals(node)) {
                                                        response = elementCFDI5.getAttributeValue(element);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
        response="";
        }
        return response;
    }
}
