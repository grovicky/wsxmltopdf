package com.bmv.wsxmltopdf.utils;
/*ConfigGlobalGasPropertiespackage integra.globalgas.com.esb.util;*/

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigGlobalGasProperties {
   /*
    pojo.esb.loggin.xml=//ruta
    pojo.esb.loggin.jasper.pdf=//ruta
    */

    private String path;
    private int maxFileSize;
    private int maxFilesRetained;

    private     Properties prop;
    private String xml;
    private String jasper;
    private InputStream input;
    public ConfigGlobalGasProperties() {
        prop = new Properties();

        try {
            String val="/home/yuri/Descargas/ConfigGlobalGas.properties";
            input = new FileInputStream(val);
            prop.load(input);
            this.path = prop.getProperty("pojo.esb.logging.path");
            this.xml= prop.getProperty("pojo.esb.loggin.xml");
            this.jasper= prop.getProperty("pojo.esb.loggin.jasper.pdf");
            this.maxFileSize = Integer.parseInt(  prop.getProperty("pojo.esb.logging.maxFileSize"));
            this.maxFilesRetained = Integer.parseInt(  prop.getProperty("pojo.esb.logging.maxFilesRetained"));

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getXml() {
        return xml;
    }

    public String getJasper() {
        return jasper;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getMaxFileSize() {
        return maxFileSize;
    }

    public void setMaxFileSize(int maxFileSize) {
        this.maxFileSize = maxFileSize;
    }

    public int getMaxFilesRetained() {
        return maxFilesRetained;
    }

    public void setMaxFilesRetained(int maxFilesRetained) {
        this.maxFilesRetained = maxFilesRetained;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

    public void setJasper(String jasper) {
        this.jasper = jasper;
    }
}